const inventory = require("./Inventory");

function problem1(inventory,carID) {
    const result = ""
    if( (typeof(carID)) === undefined || (typeof(inventory)) === undefined || (inventory.length) === 0 ) {
        return [];
    };
    if (inventory.length > 0){
        for (i = 0; i < inventory.length; i++){
            if(inventory[i].id === carID) {
                result = inventory[i];
            }
        }
        return result;
    }
}

module.exports = problem1;