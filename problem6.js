const inventory = require("./Inventory");

let problem6 = [];
for (i=0; i<inventory.length; i++) {
    let brand = inventory[i].car_make;
    if (brand === "BMW" || brand ==="Audi") {
        problem6.push(inventory[i]);
    }
}

module.exports = problem6;