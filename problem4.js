const inventory = require("./Inventory");

function problem4(inventory) {
    let carYears = [];
    for (i = 0; i<inventory.length; i++){
       let year = inventory[i].car_year;
        carYears.push(year);
    }
    return carYears;
}


module.exports = problem4;